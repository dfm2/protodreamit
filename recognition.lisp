;;; Copyright (c) 2017 Carnegie Mellon University
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;;; software and associated documentation files (the "Software"), to deal in the Software
;;; without restriction, including without limitation the rights to use, copy, modify,
;;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so, subject to the following
;;; conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in all copies
;;; or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
;;; CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
;;; OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(in-package :cl-user)                   ; Yuk. ACT-R makes other packages too difficult.

(def-dreamit-type :track tag time field encoding maneuver)

(defcomponent :recognition (json :data
                                 :pattern ((encoding . "aggregate"))
                                 :out-type :track)
  (with-model recognition
    (let* ((tag (cdr (assoc 'tag json)))
           (time (cdr (assoc 'time json)))
           (field (cdr (assoc 'field json)))
           (chunk (add-data-chunk tag time field (cdr (assoc 'value json)) 'aggregate)))
      (when (> time 0)
        (schedule-set-buffer-chunk 'visual chunk 0.0)
        (run 10.0)
        (when-let ((result (get-result-value 'maneuver)))
          (and result
               `(((tag . ,tag)
                  (time . ,time)
                  (x . ,(get-result-value 'x))
                  (y . ,(get-result-value 'y))
                  (z . ,(get-result-value 'z))
                  (heading . ,(get-result-value 'heading))
                  (encoding . "aggregate")
                  (maneuver . ,result)))))))))

(defparameter *recognition-updates*
  `((("1002" "x") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1002" "y") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1002" "z") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1002" "heading") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1003" "x") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1003" "y") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1003" "z") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1003" "heading") . ,(make-array 101 :element-type 'bit :initial-element 0))))

(defparameter *recognition-sleep-time* 0.5)

(defcomponent :recognition-pull (json :data
                                      :out-type :track
                                      :other-thread pull-recognition-data)
  (when (loop for (nil . bv) in *recognition-updates*
              always (loop for i from 1 to 100
                           never (zerop (bit bv i))))
    (setf *recognition-sleep-time* 10000)) ; stop pulling
  (let* ((tag (cdr (assoc 'tag json)))
         (time (cdr (assoc 'time json)))
         (field (cdr (assoc 'field json)))
         (update (cdr (assoc (list tag field) *recognition-updates* :test #'equalp))))
    (when (zerop (bit update time))
      (setf (bit update time) 1)
      (with-model recognition
        (let ((chunk (add-data-chunk tag time field (cdr (assoc 'value json)) 'aggregate)))
          (when (> time 0)
            (schedule-set-buffer-chunk 'visual chunk 0.0)
            (run 10.0)
            (when-let ((result (get-result-value 'maneuver)))
              (and result
                   `(((tag . ,tag)
                      (time . ,time)
                      (x . ,(get-result-value 'x))
                      (y . ,(get-result-value 'y))
                      (z . ,(get-result-value 'z))
                      (heading . ,(get-result-value 'heading))
                      (encoding . "aggregate")
                      (maneuver . ,result)))))))))))

(defun pull-recognition-data ()
  (loop
    (send-message '((encoding . "aggregate")) "recognition-pull" :data
                  (cdr (assoc "recognition-pull" *client-component-streams* :test #'equalp))
                  (cdr (assoc "recognition-pull" *component-locks* :test #'equalp)))
    (sleep *recognition-sleep-time*)))

;;; Model

(define-model recognition

(sgp :esc t :bll 0.5 :tmp 1.0 :rt -10.0 :v t)

(chunk-type data tag time field value encoding)
(chunk-type track tag time x y z heading encoding maneuver)

(add-dm
 (x isa chunk)
 (y isa chunk)
 (z isa chunk)
 (heading isa chunk)
 (aggregate isa chunk)
 (uturn isa chunk)
 (descending-uturn isa chunk))

(p* recognition-create-track-store-field
   =visual>
      isa data
      tag =tag
      time =time
      field =field
      value =value
      encoding aggregate
   ?imaginal>
      state free
==>
   !output! (Creating Integrated Chunk Tag =tag Time =time Field =field Value =value)
   +imaginal>
      isa track
      tag =tag
      time =time
      =field =value
      encoding aggregate)

(p* recognition-store-field
   =visual>
      isa data
      tag =tag
      time =time
      field =field
      value =value
      encoding aggregate
   =imaginal>
      isa track
      tag =tag
      time =time
==>
   !output! (Adding Field =field Value =value)
   =imaginal>
      =field =value)

(p recognition-uturn
   =imaginal>
      isa track
      tag =tag
      time =time
   <  heading -5
      encoding aggregate
      maneuver nil
==>
   !output! (Labeling Track as Descending U-Turn)
   =imaginal>
      maneuver uturn)

(p recognition-descending-uturn
   =imaginal>
      isa track
      tag =tag
      time =time
   <  z -100
   <  heading -5
      encoding aggregate
      maneuver nil
==>
   !output! (Labeling Track as Descending U-Turn)
   =imaginal>
      maneuver descending-uturn)

)
