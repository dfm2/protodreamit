This is a prototype decomposition of the DREAMIT phase 1 sample ACT-R
model into a workspace and several compoents, communicating with each
other using JSON over TCP.

This is a prototype. This is only a prototype. Had this been a real
application a lot more effort would have had to have been expended on
making it more robust. As it is, if you send it bad input, it will
vomit blood. Once some of the components have finished they will
scream in pain after timing out. If you don't properly drive a stake
through the workspace's heart when you're done it will chase after you
trying to bite you in the neck. You have been warned.

It is structured as a (primitive, prototype) workspace, and four
components, each running in a different process. Each of the
components communicates only with the workspace. The workspace
functions both as a switchboard, routing incoming messages to other
components that have subscribed to it; and as a database, recording
all incoming messages and allowing components to query it.

When the workspace starts it listens on a designated port (8833 by
default) for incoming connections from components. When a component
connects it first sends a JSON message that is a pattern describing
what, if any, messages it would like to subscribe to. The workspace
then continues listening to that component, records any messages sent
by it, forwards those messages it receives to any components that have
subscribed to such messages, and replies to any queries from
components.

The four components are named simulation, differentiation, integration
and recognition; there are also variations on that last three named
differentiation-pull, integration-pull and recognition-pull. The
simulation component reads a CSV data file, and sends messages with
the absolute positions of the two aircraft to the workspace. There is
some crude noise to the the timing in which the simulation component
sends data to the workspace. However, for any object the data is
guaranteed to come in order, and without omissions. The remaining
three components are sent messages as a result of subscriptions or
queries, and publish their results, which are typically then read by
another module as a result of its subscription.

The JSON messages passed to and from the workspace must all be exactly
one line of text: they must end with a linefeed, and contain no
embedded linefeeds.

The workspace writes to standard out a transcript of all incoming and
outgoing messages. To read this transcript note that:

- Each line is preceded by < for messages being received by the
  workspace from a component, and > for messages being sent by the
  workspace to a component.

- If an incoming, <, message is an initial subscription the < is
  followed by S.

- Outgoing, >, messages have the > followed by S or Q, indicating
  whether it is being sent in response to a subscription or an
  explicit query from the component.

- This is followed by the text of the JSON message itself.

- The sender of an incoming, <, message can be determined from the
  dreamit:sender slot of the message. For outgoing, >, messages the
  target component is named in parentheses following the >[SQ].

Note that the workspace spawns a thread to listen to each of the
sockets connecting it to a component. This thread mostly just reads
messages from the socket and then adds them to a queue, from which it
is extracted and processed by the workspace's main thread.

The bash scripts run.sh and run-pull.sh are conveniences for setting
up the five separate processes need to run this prototype. They open a
separate windows for each process, load the code, and run either the
workspace or a component. It is important that the workspace be
started first, and that the simulation component be started last. The
script reads a line from the terminal before launching the simulation
component to allow the user to ensure correct timing.

As of 21 February 2017 the numeric results of this prototype are
suspect. They appear to diverge from those of the original, monolithic
phase 1 sample. Christian and I am trying to figure out what's going
on.
