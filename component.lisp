;;; Copyright (c) 2017 Carnegie Mellon University
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;;; software and associated documentation files (the "Software"), to deal in the Software
;;; without restriction, including without limitation the rights to use, copy, modify,
;;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so, subject to the following
;;; conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in all copies
;;; or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
;;; CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
;;; OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(in-package :cl-user)                   ; Yuk. ACT-R makes other packages too difficult.

(defparameter *components* (make-hash-table :test #'equalp))

(defparameter +default-timeout+ 30)

(defparameter *component-locks* '())

(defparameter *client-component-streams* '())

(defparameter *component-runtimes* (make-hash-table :test #'equalp))

(defmacro defcomponent (name (var type &key pattern
                                  (out-type type)
                                  arguments
                                  host
                                  port
                                  timeout
                                  other-thread)
                        &body body)
  `(setf (gethash ',(string-downcase name) *components*)
         #'(lambda (,@arguments)
             (%run-component ',(string-downcase name)
                             ,(not (null var))
                             ',type
                             ',pattern
                             ',out-type
                             ',host
                             ',port
                             ',timeout
                             ',other-thread
                             #'(lambda (,(or var '#0=#:ignore))
                                 (declare (ignorable #0#))
                                 ,@body)))))

(defun %run-component (name takes-input-p type pattern out-type host port timeout
                       other-thread thunk)
  (let ((lock (bt:make-lock (format nil "~A-lock" name))))
    (push (cons name lock) *component-locks*)
    (usocket:with-client-socket (socket
                                 stream
                                 (or host "localhost")
                                 (or port +default-port+)
                                 :timeout (or timeout +default-timeout+))
      (push (cons name stream) *client-component-streams*)
      (send-message pattern name type stream lock)
      (when other-thread
        (bt:make-thread other-thread :name (format nil "~A-other-thread" name)))
      (with-runtime ((gethash name *component-runtimes* 0))
        (loop (loop for json in (funcall thunk
                                         (and takes-input-p
                                              (cdr (assoc 'arguments
                                                          (decode-json (read-line stream))))))
                    when json
                    do (send-message json name out-type stream lock)))))))

(defparameter *message-index* 0)

(defun send-message (json component type stream lock)
  (bt:with-lock-held (lock)
    (format stream "~A~%"
            (encode-json `((name . ,(format nil "~A-~A" component (incf *message-index*)))
                           (time . ,(get-universal-time))
                           (sender . ,(string-downcase component))
                           (type . ,(string-downcase type))
                           (arguments . ,json))))
    (finish-output stream)))

(defun run-component (name &rest arguments)
  (apply (gethash (string-downcase name) *components*) arguments))
