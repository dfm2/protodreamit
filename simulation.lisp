;;; Copyright (c) 2017 Carnegie Mellon University
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;;; software and associated documentation files (the "Software"), to deal in the Software
;;; without restriction, including without limitation the rights to use, copy, modify,
;;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so, subject to the following
;;; conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in all copies
;;; or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
;;; CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
;;; OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(in-package :cl-user)                   ; Yuk. ACT-R makes other packages too difficult.

(def-dreamit-type :data tag time field value encoding)

(defparameter *data* nil)

(defcomponent :simulation (nil :data :arguments (data-file))
  (unless *data*
    (extract-data data-file))
  (let ((data (if (cdar *data*)
                  (if (cdadr *data*)
                      (nth (random 2) *data*)
                      (first *data*))
                  (and (cdadr *data*) (second *data*)))))
    (if data
        (let ((time (car data)) (values (pop (cdr data))))
          (incf (car data))
          (prog1 (let ((tag (format nil "~A" (pop values))))
                   (loop for attribute in '(x y z heading)
                         for v in values
                         collect `((tag . ,tag)
                                   (time . ,time)
                                   (field . ,(string-downcase attribute))
                                   (value . ,v)
                                   (encoding . "absolute"))))
            (when (> (random 4) 0)
              (sleep (* 0.05 (random 40))))))
        (sleep 10000))))                ; all done, just hang out

(defun extract-data (data-file)
  (let* ((defaults (merge-pathnames "data/data.csv"
                                   (asdf:system-source-directory :protodreamit)))
         (data (load-data (merge-pathnames (string-downcase data-file) defaults)
                          :fields '(Tail_Number x y z heading))))
    (loop for i from 0 below (array-dimension data 0)
          for list = (loop for j from 0
                           for attribute in '(tag x y z heading)
                           collect (aref data i j))
          when (= (mod i 3) 1)
          collect list into object-1
          when (= (mod i 3) 2)
          collect list into object-2
          finally (setf *data* (list (cons 0 object-1) (cons 0 object-2))))))
