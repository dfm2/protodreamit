;;; Copyright (c) 2017 Carnegie Mellon University
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;;; software and associated documentation files (the "Software"), to deal in the Software
;;; without restriction, including without limitation the rights to use, copy, modify,
;;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so, subject to the following
;;; conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in all copies
;;; or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
;;; CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
;;; OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;; ACT-R has a funky relationship to the surrounding Lisp. It really does not like being
;; loaded anywhere but CL-USER, and has its own notion of keeping FASL files around, which
;; further infuriates the package system. When changing Lisp implementations it will
;; likely be necessary to push :actr-recompile onto *features*, building it once, and then
;; commenting out the push again. Sigh. TODO Does this need to be wrapped in eval-when?

;; Anyway, ACT-R makes using other packages so difficult, we're just going to keep
;; everything in cl-user. Yuk. Means I can't use iterate, either, so things will be badly
;; loop-infested.

(in-package :cl-user)

; (push :actr-recompile *features*)

(push :clean-actr *features*)

(eval-when (:execute :load-toplevel :compile-toplevel)
  (unless (member :act-r-6.0 *features*)
    (load (merge-pathnames "actr6/load-act-r-6"
                           (asdf:system-source-directory :protodreamit)))))

(clear-all)

(shadowing-import '(alexandria:when-let
                    alexandria:nconcf
                    alexandria:make-keyword))

(defparameter +default-port+ 8833)

(defun decode-json (string)
  (voorhees::unpack-json (st-json:read-json-from-string string)))

(defun encode-json (s-expr)
  (st-json:write-json-to-string (voorhees::pack-json s-expr)))

(defun add-data-chunk (tag time field value encoding)
  (first (add-dm-fct `((,(intern (format nil "~@{~:(~A~)~^-~}" tag time field encoding))
                         isa data
                         tag ,tag
                         time ,time
                         field ,field
                         value ,value
                         encoding ,encoding)))))

(defun get-result-value (slot)
  (chunk-slot-value-fct (first (no-output (buffer-chunk imaginal))) slot))

#+ccl
(setf *random-state* #.(CCL::INITIALIZE-MRG31K3P-STATE 904862392 652530637 2046546480
                                                       84322031 1548193404 1475677467))

(defmacro with-runtime ((sum) &body body)
  `(multiple-value-bind (result runtime) (%measure-runtime #'(lambda () ,@body))
     (incf ,sum runtime)
     result))

(defun %measure-runtime (thunk)
  (let* ((start (get-internal-run-time))
         (result (funcall thunk))
         (end (get-internal-run-time)))
    (values result (- end start))))
