;;; Copyright (c) 2017 Carnegie Mellon University
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;;; software and associated documentation files (the "Software"), to deal in the Software
;;; without restriction, including without limitation the rights to use, copy, modify,
;;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so, subject to the following
;;; conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in all copies
;;; or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
;;; CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
;;; OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(in-package :asdf-user)

(defsystem :protodreamit
  :version "0.1"
  :license "MIT"
  :author "Don Morrison <dfm2@cmu.edu>"
  :depends-on (:alexandria
               :bordeaux-threads
               :iterate
               :st-json
               :usocket
               :voorhees)
  :components ((:file "protodreamit")
               (:file "csv-parse-utility" :depends-on ("protodreamit"))
               (:file "workspace" :depends-on ("protodreamit"))
               (:file "component" :depends-on ("protodreamit"))
               (:file "simulation" :depends-on ("protodreamit" "csv-parse-utility" "component"))
               (:file "differentiation" :depends-on ("protodreamit" "component"))
               (:file "integration" :depends-on ("protodreamit" "component"))
               (:file "recognition" :depends-on ("protodreamit" "component"))))
