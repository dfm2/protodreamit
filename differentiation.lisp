;;; Copyright (c) 2017 Carnegie Mellon University
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;;; software and associated documentation files (the "Software"), to deal in the Software
;;; without restriction, including without limitation the rights to use, copy, modify,
;;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so, subject to the following
;;; conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in all copies
;;; or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
;;; CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
;;; OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(in-package :cl-user)                   ; Yuk. ACT-R makes other packages too difficult.

(defcomponent :differentiation (json :data :pattern ((encoding . "absolute")))
  (with-model differentiation
    (let* ((tag (cdr (assoc 'tag json)))
           (time (cdr (assoc 'time json)))
           (field (cdr (assoc 'field json)))
           (chunk (add-data-chunk tag time field (cdr (assoc 'value json)) 'absolute)))
      (when (> time 0)
        (schedule-set-buffer-chunk 'visual chunk 0.0)
        (run 10.0)
        `(((tag . ,tag)
           (time . ,time)
           (field . ,field)
           (value . ,(get-result-value 'value))
           (encoding . "relative")))))))

(defparameter *differentiation-updates*
  `((("1002" "x") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1002" "y") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1002" "z") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1002" "heading") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1003" "x") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1003" "y") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1003" "z") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1003" "heading") . ,(make-array 101 :element-type 'bit :initial-element 0))))

(defparameter *differentiation-sleep-time* 0.5)

(defcomponent :differentiation-pull (json :data :other-thread pull-differentiation-data)
  (when (loop for (nil . bv) in *differentiation-updates*
              always (loop for i from 0 to 100
                           never (zerop (bit bv i))))
    (setf *differentiation-sleep-time* 10000)) ; stop pulling
  (let* ((tag (cdr (assoc 'tag json)))
         (time (cdr (assoc 'time json)))
         (field (cdr (assoc 'field json)))
         (update (cdr (assoc (list tag field) *differentiation-updates* :test #'equalp))))
    (when (zerop (bit update time))
      (setf (bit update time) 1)
      (with-model differentiation
        (let ((chunk (add-data-chunk tag time field (cdr (assoc 'value json)) 'absolute)))
          (when (> time 0)
            (schedule-set-buffer-chunk 'visual chunk 0.0)
            (run 10.0)
            `(((tag . ,tag)
               (time . ,time)
               (field . ,field)
               (value . ,(get-result-value 'value))
               (encoding . "relative")))))))))

(defun pull-differentiation-data ()
  (loop
    (send-message '((encoding . "absolute")) "differentiation-pull" :data
                  (cdr (assoc "differentiation-pull" *client-component-streams* :test #'equalp))
                  (cdr (assoc "differentiation-pull" *component-locks* :test #'equalp)))
    (sleep *differentiation-sleep-time*)))

;;; Model

(define-model differentiation

(sgp :esc t :bll 0.5 :tmp 1.0 :rt -10.0 :v t)

(chunk-type data tag time field value encoding)

(add-dm
 (x isa chunk)
 (y isa chunk)
 (z isa chunk)
 (heading isa chunk)
 (absolute isa chunk)
 (relative isa chunk))

(p differentiation-retrieval
   =visual>
      isa data
      tag =tag
      time =time
      field =field
      value =current
      encoding absolute
   ?retrieval>
      state free
==>
   !bind! =previous-time (1- =time) ;;; previous step
   !output! (Retrieving Data Tag =tag Time =time Field =field)
   +imaginal>
      =visual
   +retrieval>
      isa data
      tag =tag
      time =previous-time
      field =field
      encoding absolute)

(p differentiation-error
   =imaginal>
      isa data
      tag =tag
      time =time
      field =field
      value =current
      encoding absolute
   ?retrieval>
      state error
==>
   !output! (Retrieving Data Tag =tag Time =time Field =field Error)
   -retrieval>)

(p differentiation-computation
   =imaginal>
      isa data
      tag =tag
      time =time
      field =field
      value =current
      encoding absolute
   =retrieval>
      isa data
      tag =tag
      time =previous-time
      field =field
      value =past
      encoding absolute
   !eval! (= =time (1+ =previous-time)) ;;; consecutive steps
==>
   !bind! =delta (- =current =past)
   !output! (Retrieved Data Tag =tag Time =previous-time Field =field Value =past Delta =delta)
   =imaginal>
      value =delta
      encoding relative)
)
