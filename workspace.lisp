;;; Copyright (c) 2017 Carnegie Mellon University
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;;; software and associated documentation files (the "Software"), to deal in the Software
;;; without restriction, including without limitation the rights to use, copy, modify,
;;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so, subject to the following
;;; conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in all copies
;;; or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
;;; CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
;;; OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(in-package :cl-user)                   ; Yuk. ACT-R makes other packages too difficult.

(defparameter +meta-attributes+
  (loop for n in '("NAME" "TIME" "SENDER" "TYPE")
        collect (intern (format nil "DREAMIT:~A" n))))

(defparameter *dreamit-types* (make-hash-table :test #'equalp))

(defparameter *database* '())

(defparameter *subscriptions* '())

(defparameter *component-streams* (make-hash-table :test #'equalp))

;; When a component registers itself that ends up being handled in the thread that
;; listens for that component, rather than the main thread, so make sure the output
;; doesn't get garbled.

(defparameter *standard-output-lock* (bt:make-lock "STANDARD-OUTPUT-LOCK"))

;; There's a separate thread listening for each component that's registered itself. The
;; bulk of what it does is just read a JSON value and shoves it on a queue for the main
;; thread to process.

(defparameter *message-queue* '())

(defparameter *message-queue-condition* (bt:make-condition-variable))

(defparameter *message-queue-lock* (bt:make-lock "MESSAGE-QUEUE-LOCK"))

(defparameter *message-count* 0)

(defparameter *message-size* 0)

(defparameter *workspace-runtime* 0)

(defun enqueue-message (message)
  (bt:with-lock-held (*message-queue-lock*)
    (nconcf *message-queue* (list message))
    (bt:condition-notify *message-queue-condition*)))

(defmacro def-dreamit-type (name &rest attributes)
  `(setf (gethash ',(string-downcase name) *dreamit-types*) ',attributes))

(defun run-workspace (&key (port +default-port+))
  ;; Were this more than a rough prototype we should capture the return values of the
  ;; following and ensure things are shut down correctly; instead we clean up by
  ;; killing and restarting Lisp.
  (usocket:socket-server nil port #'component-connection '()
                         :in-new-thread t
                         :multi-threading t)
  (bt:with-lock-held (*message-queue-lock*)
    (loop
      (with-runtime (*workspace-runtime*)
        (loop while *message-queue*
              for message = (pop *message-queue*)
              do (process-message message)))
      (bt:condition-wait *message-queue-condition* *message-queue-lock*))))

(defun get-component-name (decoded-json)
  (cdr (assoc 'sender decoded-json)))

(defun get-dreamit-type (decoded-json)
  (cdr (assoc 'type decoded-json)))

(defun component-connection (stream)
  (let* ((message (read-line stream))
         (decoded (decode-json message))
         (component (get-component-name decoded)))
    (setf (gethash component *component-streams*) stream)
    (when (loop with args = (cdr (assoc 'arguments decoded))
                for attribute in (gethash (get-dreamit-type decoded) *dreamit-types*)
                thereis (assoc attribute args))
      (process-message message t))
    (loop (enqueue-message (read-line stream)))))

(defun match-p (pattern subject)
  (and (equalp (get-dreamit-type pattern) (get-dreamit-type subject))
       (loop with pattern-args = (cdr (assoc 'arguments pattern))
             with subject-args = (cdr (assoc 'arguments subject))
             for attribute in (gethash (get-dreamit-type pattern) *dreamit-types*)
             for pair = (assoc attribute pattern-args)
             always (or (null pair)
                        (equalp (cdr pair) (cdr (assoc attribute subject-args)))))))

(defun query-p (json)
  (loop with args = (cdr (assoc 'arguments json))
        for attribute in (gethash (get-dreamit-type json) *dreamit-types*)
        thereis (null (assoc attribute args))))

(defun process-message (message &optional subscription-p)
  (incf *message-count*)
  (incf *message-size* (length message))
  (bt:with-lock-held (*standard-output-lock*)
    (format t "<~:[ ~;S~] ~A~%" subscription-p message))
  (let ((decoded (decode-json message)))
    (cond (subscription-p
           (push decoded *subscriptions*))
          ((query-p decoded)
           (loop for m in (reverse *database*)
                 for d = (decode-json m)
                 when (match-p decoded d)
                 do (send-workspace-message (get-component-name decoded) d nil)))
          (t
           (loop for pattern in *subscriptions*
                 when (match-p pattern decoded)
                 do  (send-workspace-message (get-component-name pattern) decoded t))
           (push message *database*)))))

(defun send-workspace-message (component json query-result-p)
  (incf *message-count*)
  (let ((message (encode-json json))
        (stream (gethash component *component-streams*)))
    (incf *message-size* (length message))
    (bt:with-lock-held (*standard-output-lock*)
      (format t ">~:[Q~;S~] (~(~A~)) ~A~%" query-result-p component message))
    (format stream "~A~%" message)
    (finish-output stream)))
