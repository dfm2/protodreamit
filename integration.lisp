;;; Copyright (c) 2017 Carnegie Mellon University
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;;; software and associated documentation files (the "Software"), to deal in the Software
;;; without restriction, including without limitation the rights to use, copy, modify,
;;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so, subject to the following
;;; conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in all copies
;;; or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
;;; CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
;;; OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(in-package :cl-user)                   ; Yuk. ACT-R makes other packages too difficult.

(defcomponent :integration (json :data :pattern ((encoding . "relative")))
  (with-model integration
    (let* ((tag (cdr (assoc 'tag json)))
           (time (cdr (assoc 'time json)))
           (field (cdr (assoc 'field json)))
           (chunk (add-data-chunk tag time field (cdr (assoc 'value json)) 'relative)))
      (when (> time 0)
        (schedule-set-buffer-chunk 'visual chunk 0.0)
        (run 10.0)
        `(((tag . ,tag)
           (time . ,time)
           (field . ,field)
           (value . ,(get-result-value 'value))
           (encoding . "aggregate")))))))

(defparameter *integration-updates*
  `((("1002" "x") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1002" "y") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1002" "z") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1002" "heading") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1003" "x") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1003" "y") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1003" "z") . ,(make-array 101 :element-type 'bit :initial-element 0))
    (("1003" "heading") . ,(make-array 101 :element-type 'bit :initial-element 0))))

(defparameter *integration-sleep-time* 0.5)

(defcomponent :integration-pull (json :data :other-thread pull-integration-data)
  (when (loop for (nil . bv) in *integration-updates*
              always (loop for i from 1 to 100
                           never (zerop (bit bv i))))
    (setf *integration-sleep-time* 10000)) ; stop pulling
  (let* ((tag (cdr (assoc 'tag json)))
         (time (cdr (assoc 'time json)))
         (field (cdr (assoc 'field json)))
         (update (cdr (assoc (list tag field) *integration-updates* :test #'equalp))))
    (when (zerop (bit update time))
      (setf (bit update time) 1)
      (with-model integration
        (let ((chunk (add-data-chunk tag time field (cdr (assoc 'value json)) 'relative)))
          (when (> time 0)
            (schedule-set-buffer-chunk 'visual chunk 0.0)
            (run 10.0)
            `(((tag . ,tag)
               (time . ,time)
               (field . ,field)
               (value . ,(get-result-value 'value))
               (encoding . "aggregate")))))))))

(defun pull-integration-data ()
  (loop
    (send-message '((encoding . "relative")) "integration-pull" :data
                  (cdr (assoc "integration-pull" *client-component-streams* :test #'equalp))
                  (cdr (assoc "integration-pull" *component-locks* :test #'equalp)))
    (sleep *integration-sleep-time*)))

;;; Model

(define-model integration

(sgp :esc t :bll 0.5 :tmp 1.0 :rt -10.0 :v t)

(chunk-type data tag time field value encoding)

(add-dm
 (x isa chunk)
 (y isa chunk)
 (z isa chunk)
 (heading isa chunk)
 (relative isa chunk)
 (aggregate isa chunk))

(p integration-retrieval
   =visual>
      isa data
      tag =tag
      time =time
      field =field
      value =current
      encoding relative
   ?blending>
      state free
==>
   !output! (Blending over Data Tag =tag Time =time Field =field)
   +imaginal>
      =visual
   +blending>
      isa data
      tag =tag
      time =time
      field =field
      encoding relative)

(p integration-error
   =imaginal>
      isa data
      tag =tag
      time =time
      field =field
      value =current
      encoding relative
   ?blending>
      state error
==>
   !output! (Blending over Data Tag =tag Time =time Field =field Error)
   -blending>)

(p integration-computation
   =imaginal>
      isa data
      tag =tag
      time =time
      field =field
      value =current
      encoding relative
   =blending>
      isa data
      tag =tag
;      time =time   ;;; doesn't have to be exact
      field =field
      encoding relative
      value =aggregate
==>
   !output! (Blending Data Tag =tag Time =time Field =field Value =current Aggregate =aggregate)
   +imaginal>
      value =aggregate
      encoding aggregate)
)
