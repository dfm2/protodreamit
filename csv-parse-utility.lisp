(in-package :cl-user)                   ; Yuk. ACT-R makes other packages too difficult.

(defparameter *directory* *load-pathname*)

(defun load-data (file &key (fields nil) (conditions nil) (array t) (trace nil))
  "Read file and return specified fields for lines satisfying conditions.
  Fields contains a list of desired headers, nil for no header line, or t for all.
  Conditions contains a list of headers, comparison operator and value, e.g., (angle < 75).
  Returns list of lists or array."
  (let ((data nil)
        (pathname (make-pathname :directory (or (pathname-directory file) (pathname-directory *directory*))
                                 :name (pathname-name file)
                                 :type (pathname-type file))))
    (with-open-file (input pathname :direction :input)
      (let* ((line nil)
             (length 0)
             (all-fields nil)
             (field-value nil)
             (field-index 0))
        (when fields
          (setf line (read-line input nil 'end))
          (setf length (length line))
          (loop
            (when (>= field-index length) (return))
            (multiple-value-setq (field-value field-index)
              (read-from-string line nil 'end :start field-index))
            (push field-value all-fields))
          (setf all-fields (reverse all-fields))
          (when trace (format t "All Fields: ~S~%" all-fields)))
        (when trace (format t "Fields: ~S~%" fields))
        (loop
          (let ((field nil)
                (datum nil)
                (value nil)
                (index 0)
                (save t))
            (setf line (read-line input nil 'end))
            (when (string-equal line 'end) (return))
            (setf length (length line))
            (setf field-index 0)
            (loop
              (when (>= index length) (return))
              (multiple-value-setq (value index)
                (read-from-string line t nil :start index))
              (when trace (format t "~S " value))
              (setf field (nth field-index all-fields))
              (when (and fields conditions)
                (let ((condition (assoc field conditions)))
                (when (and condition (not (funcall (second condition) value (third condition))))
                  (when trace (format t "NOT ~S ~S " (second condition) (third condition)))
                  (setf save nil))))
              (when trace (format t "~S " field))
              (when (or (null fields) (member field fields))
                (push value datum))
              (incf field-index))
            (when trace (format t "~%"))
            (when save
              (push (reverse datum) data))))
        (setf data (reverse data))
        (if array
            (make-array (list (length data) (length (first data))) :initial-contents data)
            data)))))
